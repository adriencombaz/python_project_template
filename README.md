## Template structure for python projects using Docker

### Building the Docker image

- Navigate to the docker image file: `cd docker`
- (re)Build the image: `docker compose build`
- Remove the old image: `docker system prune`  

### Running the Docker container

- Launch the container: `docker compose up` add the flag `-d` to run in detached mode
- Navigate to [localhost:8888](localhost:8888) and write and run your code in Jupyter.
- Once done, stop the container: `docker compose down` if in detached mode or `Ctrl+C` if not

### Using the Docker image in Pycharm

To use the python interpreter in PyCharm, go to `Settings`, `Python Interpreter`, `Add`, add a Docker interpreter, select the right image and the Python interpreter path should be `/opt/conda/envs/my_conda_env/bin/python`.   
Note: If you gave another name than `my_conda_env` to your environment in the [/docker/Dockerfile](./docker/Dockerfile) file, don't forget to replace it here as well.
